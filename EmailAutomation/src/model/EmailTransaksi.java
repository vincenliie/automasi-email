/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Asus
 */
public class EmailTransaksi implements Comparable{

    private int id;
    private String pengirimEmail;
    private String subjek;
    private String namaDebitur = "NA";
    private String kanwil = "NA";
    private String cabang = "NA";
    private String pic = "NA";
    private String divisiPengirim = "NA";
    private String transaksi = "NA";
    private Date waktuDiterima;
    private Date waktuOrder;
    private Date waktuPemeriksaan;
    private String durasi;

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }
    
    public String getPengirimEmail() {
        return pengirimEmail;
    }

    public void setPengirimEmail(String pengirimEmail) {
        this.pengirimEmail = pengirimEmail;
    }
    
    public String getSubjek() {
        return subjek;
    }

    public void setSubjek(String subjek) {
        this.subjek = subjek;
    }
    
    public String getNamaDebitur() {
        return namaDebitur;
    }

    public void setNamaDebitur(String namaDebitur) {
        this.namaDebitur = namaDebitur;
    }
    
    public String getKanwil() {
        return kanwil;
    }

    public void setKanwil(String kanwil) {
        this.kanwil = kanwil;
    }
    
    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }
    
    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
    
    public String getDivisiPengirim() {
        return divisiPengirim;
    }

    public void setDivisiPengirim(String divisiPengirim) {
        this.divisiPengirim = divisiPengirim;
    }

    public String getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(String transaksi) {
        this.transaksi = transaksi;
    }
    
    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public Date getWaktuDiterima() {
        return waktuDiterima;
    }

    public void setWaktuDiterima(Date waktuDiterima) {
        this.waktuDiterima = waktuDiterima;
    }

    public Date getWaktuOrder() {
        return waktuOrder;
    }

    public void setWaktuOrder(Date waktuOrder) {
        this.waktuOrder = waktuOrder;
    }

    public Date getWaktuPemeriksaan() {
        return waktuPemeriksaan;
    }

    public void setWaktuPemeriksaan(Date waktuPemeriksaan) {
        this.waktuPemeriksaan = waktuPemeriksaan;
    }

    @Override
    public int compareTo(Object compareTrx) {
        Date compareDate=((EmailTransaksi)compareTrx).getWaktuDiterima();
        /* For Ascending order*/
        return this.waktuDiterima.compareTo(compareDate);

        /* For Descending order*/
//        return compareDate.compareTo(this.waktuEmail);
    }
}

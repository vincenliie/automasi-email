/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Asus
 */
public class EmailTransaksiProperty{

    private int id;
    private SimpleStringProperty from;
    private SimpleStringProperty subject;
    private SimpleStringProperty namaDebitur;
    private SimpleStringProperty kanwil;
    private SimpleStringProperty cabang;
    private SimpleStringProperty pengirim;
    private SimpleStringProperty transaksi;
    private SimpleStringProperty orderTxn;
    private SimpleStringProperty pemeriksaanTxn;
    private SimpleStringProperty durasi;
    private SimpleStringProperty pic;
    private SimpleStringProperty waktuEmail;

    public EmailTransaksiProperty(int id, String from, String subject, String namaDebitur, String kanwil, String cabang, String pengirim, String transaksi, String orderTxn, String pemeriksaanTxn, String durasi, String pic, String waktuEmail) {
        this.id = id;
        this.from = new SimpleStringProperty(from);
        this.subject = new SimpleStringProperty(subject);
        this.namaDebitur = new SimpleStringProperty(namaDebitur);
        this.kanwil = new SimpleStringProperty(kanwil);
        this.cabang = new SimpleStringProperty(cabang);
        this.pengirim = new SimpleStringProperty(pengirim);
        this.transaksi = new SimpleStringProperty(transaksi);
        this.orderTxn = new SimpleStringProperty(orderTxn);
        this.pemeriksaanTxn = new SimpleStringProperty(pemeriksaanTxn);
        this.durasi = new SimpleStringProperty(durasi);
        this.pic = new SimpleStringProperty(pic);
        this.waktuEmail = new SimpleStringProperty(waktuEmail);
    }

    public EmailTransaksiProperty() {
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public SimpleStringProperty getFrom() {
        return from;
    }

    public void setFrom(SimpleStringProperty from) {
        this.from = from;
    }

    public SimpleStringProperty getSubject() {
        return subject;
    }

    public void setSubject(SimpleStringProperty subject) {
        this.subject = subject;
    }
    
    public SimpleStringProperty getNamaDebitur() {
        return namaDebitur;
    }

    public void setNamaDebitur(SimpleStringProperty namaDebitur) {
        this.namaDebitur = namaDebitur;
    }
    
    public SimpleStringProperty getKanwil() {
        return kanwil;
    }

    public void setKanwil(SimpleStringProperty kanwil) {
        this.kanwil = kanwil;
    }

    public SimpleStringProperty getPengirim() {
        return pengirim;
    }

    public void setPengirim(SimpleStringProperty pengirim) {
        this.pengirim = pengirim;
    }

    public SimpleStringProperty getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(SimpleStringProperty transaksi) {
        this.transaksi = transaksi;
    }
    
    public SimpleStringProperty getOrderTxn() {
        return orderTxn;
    }

    public void setOrderTxn(SimpleStringProperty orderTxn) {
        this.orderTxn = orderTxn;
    }

    public SimpleStringProperty getPemeriksaanTxn() {
        return pemeriksaanTxn;
    }

    public void setPemeriksaanTxn(SimpleStringProperty pemeriksaanTxn) {
        this.pemeriksaanTxn = pemeriksaanTxn;
    }

    public SimpleStringProperty getDurasi() {
        return durasi;
    }

    public void setDurasi(SimpleStringProperty durasi) {
        this.durasi = durasi;
    }
    
    public SimpleStringProperty getCabang() {
        return cabang;
    }

    public void setCabang(SimpleStringProperty cabang) {
        this.cabang = cabang;
    }
    
    public SimpleStringProperty getPic() {
        return pic;
    }

    public void setPic(SimpleStringProperty pic) {
        this.pic = pic;
    }

    public SimpleStringProperty getWaktuEmail() {
        return waktuEmail;
    }

    public void setWaktuEmail(SimpleStringProperty waktuEmail) {
        this.waktuEmail = waktuEmail;
    }

    
}

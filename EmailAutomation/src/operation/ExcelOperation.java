/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operation;

import model.EmailTransaksi;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.StageStyle;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Asus
 */
public class ExcelOperation {

    private static final String TAG = "Excel Controller";

//    public ArrayList<DebtorTransaction> importFromExcel() {
//        boolean getData;
//
//        ArrayList<DebtorTransaction> trxs = new ArrayList();
//
//        try {
//            String fileLoc = "howtodoinjava_demo.xlsx";
//
//            // Creating a Workbook from an Excel file (.xls or .xlsx)
//            Workbook workbook = WorkbookFactory.create(new File(fileLoc));
//
//            // Getting the Sheet at index zero
//            Sheet sheet = workbook.getSheetAt(0);
//
//            // Create a DataFormatter to format and get each cell's value as String
//            DataFormatter dataFormatter = new DataFormatter();
//
//            TimeOperation timeOperation = new TimeOperation();
//            int rownum = 1;
//            for (Row row : sheet) {
//                getData = true;
//
//                EmailTransaksi trx = new EmailTransaksi();
//                int colnum = 1;
//                for (Cell cell : row) {
//                    String cellValue = dataFormatter.formatCellValue(cell);
//
//                    if (rownum > 1 && colnum > 1) {
//                        switch (colnum) {
//                            case 2:
//                                trx.setNamaDebitur(cellValue);
//                                break;
//                            case 3:
//                                trx.setKanwil(cellValue);
//                                break;
//                            case 4:
//                                trx.setCabang(cellValue);
//                                break;
//                            case 5:
//                                trx.setPic(cellValue);
//                                break;
//                            case 6:
//                                trx.setDivisiPengirim(cellValue);
//                                break;
//                            case 7:
//                                trx.setTransaksi(cellValue);
//                                break;
//                            case 8:
//                                Date receivedDate = timeOperation.setStringToDate(cellValue);
//                                trx.setWaktuDiterima(receivedDate);
//                                break;
//                            case 9:
//                                if (cellValue != "") {
//                                    Date orderDate = timeOperation.setStringToDate(cellValue);
//                                    trx.setWaktuOrder(orderDate);
//                                }
//                                break;
//                            case 10:
//                                if (cellValue != "") {
//                                    getData = false;
//                                }
//                                break;
//                            default:
//                        }
//                    }
//                    colnum++;
//                }
//
//                if (rownum > 1 && getData == true) {
//                    trxs.add(trx);
//                }
//
//                rownum++;
//            }
//
//            // Closing the workbook
//            workbook.close();
//        } catch (IOException ex) {
//            Logger.getLogger(ExcelOperation.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (EncryptedDocumentException ex) {
//            Logger.getLogger(ExcelOperation.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException ex) {
//            Logger.getLogger(ExcelOperation.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return trxs;
//    }

    public XSSFCellStyle fontFormat(XSSFWorkbook workbook) {
        //Create new font
        XSSFFont font = workbook.createFont();
        font.setBold(true);

        //Set font into style
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);

        return style;
    }

    public boolean exportToExcel(Object[] trxs) throws FileNotFoundException, IOException {
        boolean exportStatus = false;
        
        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Transaksi Debitur");

        //This data needs to be written (Object[])
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[]{"No", "Nama Debitur", "Kanwil", "Cabang", "PIC", "Pengirim", "Transaksi", "Jam Email", "Order Txn", "Pemeriksaan Txn", "Durasi"});
        for (int i = 0; i < trxs.length; i++) {
            EmailTransaksi trx = (EmailTransaksi) trxs[i];
            data.put("" + (2 + i), new Object[]{"" + (1 + i), trx.getNamaDebitur(), trx.getKanwil(), trx.getCabang(), trx.getPic(), trx.getDivisiPengirim(), trx.getTransaksi(),
                ((trx.getWaktuDiterima() == null) ? "" : formatter.format(trx.getWaktuDiterima())),
                ((trx.getWaktuOrder() == null) ? "" : formatter.format(trx.getWaktuOrder())),
                ((trx.getWaktuPemeriksaan() == null) ? "" : formatter.format(trx.getWaktuPemeriksaan())),
                trx.getDurasi()});
        }

        XSSFCellStyle style = fontFormat(workbook);

        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);

                if (rownum == 1) {
                    cell.setCellStyle(style);
                }

                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }

        // Resize all columns to fit the content size
        // Column size = 11
        System.out.println();
        for (int i = 0; i < 11; i++) {
            sheet.autoSizeColumn(i);
        }

        //Get current date
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM");
        LocalDate localDate = LocalDate.now();

        System.out.println(TAG + ": " + dtf.format(localDate));

        String current_date = dtf.format(localDate);
        String fileLoc = "D:\\Laporan\\" + current_date + "\\transaksi debitur.xlsx";
        File excelFile = new File(fileLoc);
        if (!excelFile.exists()) {
            if (excelFile.getParentFile().mkdirs()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Directory not created!");
            }
        }

        //Where to write?
        if (excelFile.exists()) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle("Confirmation");
            alert.setHeaderText(excelFile.toString());
            alert.setContentText("Are you sure want to overwirte this file?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                //Write the workbook in file system
                FileOutputStream out = new FileOutputStream(excelFile);
                workbook.write(out);
                out.close();
                
                exportStatus = true;
            }
        } else {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File(fileLoc));
            workbook.write(out);
            out.close();
            exportStatus = true;
        }
        
        return exportStatus;
    }
}

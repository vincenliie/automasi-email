/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operation;

import model.EmailTransaksi;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.internet.InternetAddress;
import javax.mail.search.AndTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.SearchTerm;

/**
 *
 * @author Asus
 */
public class EmailOperation {

    public static Store store = null;

    private static final String TAG = "Email Controller : ";

    public Store connectToEmail(String host, String storeType, String email, String password) throws MessagingException {
        try {
            //create properties field
            Properties properties = new Properties();

            properties.put("mail.pop3.host", host);
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.starttls.enable", "true");

            Session emailSession = Session.getDefaultInstance(properties);

            //create the IMAP store object and connect with the imap server
            Store store = emailSession.getStore("imaps");

            store.connect(host, email, password);

            return store;
        } catch (NoSuchProviderException ex) {
            System.out.println(TAG + ": NO PROVIDER EXCEPTION");
            return null;
        }
    }

    public HashMap<Long, ArrayList<EmailTransaksi>> getUnseenUndraftedEmail(Store store) throws MessagingException {
        HashMap<Long, ArrayList<EmailTransaksi>> txnsUID = new HashMap();

        //create the folder object and open it
        Folder emailFolder = store.getFolder("INBOX");
        emailFolder.open(Folder.READ_ONLY);

        //Flag term has not seen by user
        Flags seen = new Flags(Flags.Flag.SEEN);
        FlagTerm unseen = new FlagTerm(seen, false);

        //Flag term has not drafted by user
        Flags draft = new Flags(Flags.Flag.DRAFT);
        FlagTerm undraft = new FlagTerm(draft, false);

        //Combine two of them
        SearchTerm searchTerm = new AndTerm(unseen, undraft);
        Message[] messages = emailFolder.search(searchTerm);

        UIDFolder uf = (UIDFolder) emailFolder; // cast folder to UIDFolder interface

        for (int i = messages.length - 1; i >= 0; i--) {
            Message message = messages[i];

            String emailSubject = message.getSubject().toLowerCase();
            int emailDateMonth = message.getReceivedDate().getMonth();//0 until 11

            Calendar now = Calendar.getInstance();
            int currentMonth = now.get(Calendar.MONTH);//0 until 11
            if (emailSubject.contains("txn") && emailDateMonth == currentMonth) {
                ArrayList<EmailTransaksi> txns = new ArrayList();
                try {
                    EmailTransaksi txn = new EmailTransaksi();

                    long messageId = uf.getUID(messages[i]); // get message Id of first message in the inbox
                    txn.setSubjek(message.getSubject());

                    //Get email sender
                    Address froms[] = message.getFrom();
                    InternetAddress address = (InternetAddress) froms[0];
                    String sender = address.getPersonal();
                    txn.setPengirimEmail(sender);

                    System.out.println("Waktu terima email: " + message.getReceivedDate());
                    txn.setWaktuDiterima(message.getReceivedDate());

                    txns.add(txn);

                    readContent(message, txns);

                    txnsUID.put(messageId, txns);
                } catch (Exception ex) {
                    Logger.getLogger(EmailOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        //close the store and folder objects
        emailFolder.close(false);

        return txnsUID;
    }

    public void readContent(Part p, ArrayList<EmailTransaksi> tx) throws Exception {
        if (p.isMimeType("text/plain")) {
            String isi = (String) p.getContent();

            if (isi.toLowerCase().contains("revisi")) {
                System.out.println("Ini revisi!");
                EmailTransaksi txnRevisi = new EmailTransaksi();
                txnRevisi.setWaktuDiterima(tx.get(0).getWaktuDiterima());
                txnRevisi.setPengirimEmail(tx.get(0).getPengirimEmail());
                txnRevisi.setSubjek(tx.get(0).getSubjek());
                tx.add(txnRevisi);

                //Disini ada proses pemasukkan data pada isi email kedalam class! 
                processBody(isi, tx);
            } else {
                System.out.println("Ini bukan revisi!");

                //Yang direvisi
                processBody(isi, tx);
            }
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) p.getContent();
            int count = mp.getCount();
            for (int i = 0; i < count; i++) {
                readContent(mp.getBodyPart(i), tx);
            }
        }
    }

    public void processBody(String isi, ArrayList<EmailTransaksi> tx) {
        int dataNum = 0;
        String lines[] = isi.split("\\r?\\n");

        for (int i = 0; i < lines.length; i++) {
            if (lines[i].contains(":")) {
                String data[] = lines[i].split(":");
                if (data[0].toLowerCase().contains("debitur")) {
                    String debitur = "";
                    if (data.length > 0) {
                        debitur = data[1].trim();
                    }
//                System.out.println(TAG + ": " + debitur);
                    tx.get(dataNum).setNamaDebitur(debitur);
                } else if (data[0].toLowerCase().contains("kanwil")) {
                    String kanwil = "";
                    if (data.length > 0) {
                        kanwil = data[1].trim();
                    }
//                System.out.println(TAG + ": " + kanwil);
                    tx.get(dataNum).setKanwil(kanwil);
                } else if (data[0].toLowerCase().contains("cabang")) {
                    String cabang = "";
                    if (data.length > 0) {
                        cabang = data[1].trim();
                    }
//                System.out.println(TAG + ": " + cabang);
                    tx.get(dataNum).setCabang(cabang);
                } else if (data[0].toLowerCase().contains("pengirim")) {
                    String pengirim = "";
                    if (data.length > 0) {
                        pengirim = data[1].trim();
                    }
//                System.out.println(TAG + ": " + pengirim);
                    tx.get(dataNum).setDivisiPengirim(pengirim);
                } else if (data[0].toLowerCase().contains("transaksi")) {
                    String transaksi = "";
                    if (data.length > 0) {
                        transaksi = data[1].trim();
                    }
//                System.out.println(TAG + ": " + transaksi);
                    tx.get(dataNum).setTransaksi(transaksi);
                    if (tx.size() == (dataNum + 1)) {
                        break;
                    } else {
                        dataNum++;
                    }
                } else if (data[0].toLowerCase().contains("to")) {
                    String pic = "";
                    if (data.length > 0) {
                        pic = data[1].trim();
                        if (tx.size() > 1) {//Jikalau isi email merupakan email revisi
                            tx.get(1).setPic(pic);
                        }
                        tx.get(0).setPic(pic);
                    }
//                System.out.println(TAG + ": " + pic);
                }
            }
        }
    }

    public void setMessageFlagToDraft(Store store, long uid) {
        try {
            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_WRITE);

            //Flag term has not seen by user
            Flags seen = new Flags(Flags.Flag.SEEN);
            FlagTerm unseen = new FlagTerm(seen, false);

            //Flag draft
            Flags draft = new Flags(Flags.Flag.DRAFT);
            FlagTerm undraft = new FlagTerm(draft, false);

            //Combine two of them
            SearchTerm searchTerm = new AndTerm(unseen, undraft);
            Message[] messages = emailFolder.search(searchTerm);

            UIDFolder uf = (UIDFolder) emailFolder;

            Message message = uf.getMessageByUID(uid);
            message.setFlag(Flags.Flag.DRAFT, true);

            //close the store and folder objects
            emailFolder.close(false);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(EmailOperation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(EmailOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void logOut() throws MessagingException {
        store.close();
        store = null;
    }
}

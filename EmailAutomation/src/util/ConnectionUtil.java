package util;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionUtil {
	Connection conn = null;
	public static Connection conDB() {
		try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/db_login_form", "root", "");
            return con;
		} catch (Exception e) {
			// TODO: handle exception
            System.err.println("ConnectionUtil : "+e.getMessage());
            return null;
		}
	}
}

package connection_manager;

import java.sql.*;

public class ConnectionManager {

    private static final String TAG = "ConnectionManger : ";
    public Connection con;
    private String driver = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://localhost/transaksi_debitur";
    private String username = "root";
    private String password = "";

    public Connection logOn() {
        try {
            // Load JDBC Driver
            Class.forName(driver).newInstance();
            // Buat object Connection
            con = DriverManager.getConnection(url,
                    username, password);
        } catch (Exception ex) {
            System.out.println(TAG +"Error occured when LogIn");
        }
        return con;
    }

    public void logOff() {
        try {
            // Tutup Koneksi
            con.close();
        } catch (Exception ex) {
            System.out.println("Error occured " + "when connecting to "
                    + "database");
        }
    }

    public void connect() {
        try {
            con = logOn();
        } catch (Exception e) {
            System.out.println("Error occured " + "when connecting to "
                    + "database");
        }
    }

    public void disconnect() {
        try {
            logOff();
        } catch (Exception e) {
            System.out.println(TAG + "Error occured when disconnecting to database");
        }
    }
}

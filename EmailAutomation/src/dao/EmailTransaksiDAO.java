package dao;

import connection_manager.ConnectionManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import model.EmailTransaksi;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Asus
 */
public class EmailTransaksiDAO {

    public ArrayList<EmailTransaksi> getAllDataWOEmailHeader() throws SQLException, ParseException {
        ArrayList<EmailTransaksi> txns = new ArrayList();

        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "SELECT nama_debitur, kanwil, cabang, pic, divisi_pengirim, "
                + "transaksi, waktu_diterima, waktu_order, waktu_pemeriksaan, durasi "
                + "FROM email_transaksi";

        Statement statement = conn.con.createStatement();

        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            EmailTransaksi txn = new EmailTransaksi();
            txn.setNamaDebitur(rs.getString("nama_debitur"));
            txn.setKanwil(rs.getString("kanwil"));
            txn.setCabang(rs.getString("cabang"));
            txn.setPic(rs.getString("pic"));
            txn.setDivisiPengirim(rs.getString("divisi_pengirim"));
            txn.setTransaksi(rs.getString("transaksi"));
            txn.setWaktuDiterima((Date) rs.getObject("waktu_diterima"));
            txn.setWaktuOrder(rs.getObject("waktu_order") == null
                    ? null : (Date) rs.getObject("waktu_order"));
            txn.setWaktuPemeriksaan(rs.getObject("waktu_pemeriksaan") == null
                    ? null : (Date) rs.getObject("waktu_pemeriksaan"));
            txn.setDurasi(rs.getString("durasi"));

            txns.add(txn);
        }

        conn.con.close();

        return txns;
    }

    public ArrayList<EmailTransaksi> getAllData() throws SQLException, ParseException {
        ArrayList<EmailTransaksi> txns = new ArrayList();

        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "SELECT id, pengirim_email, subjek, nama_debitur, kanwil, "
                + "cabang, pic, divisi_pengirim, transaksi, "
                + "waktu_diterima, waktu_order, waktu_pemeriksaan, durasi "
                + "FROM email_transaksi ";

        Statement statement = conn.con.createStatement();

        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            EmailTransaksi txn = new EmailTransaksi();
            txn.setId(rs.getInt("id"));
            txn.setPengirimEmail(rs.getString("pengirim_email"));
            txn.setSubjek(rs.getString("subjek"));
            txn.setNamaDebitur(rs.getString("nama_debitur"));
            txn.setKanwil(rs.getString("kanwil"));
            txn.setCabang(rs.getString("cabang"));
            txn.setPic(rs.getString("pic"));
            txn.setDivisiPengirim(rs.getString("divisi_pengirim"));
            txn.setTransaksi(rs.getString("transaksi"));
            txn.setWaktuDiterima((Date) rs.getObject("waktu_diterima"));
            txn.setWaktuOrder(rs.getObject("waktu_order") == null ? 
                    null : (Date) rs.getObject("waktu_order"));
            txn.setWaktuPemeriksaan(rs.getObject("waktu_pemeriksaan") == null ? 
                    null : (Date) rs.getObject("waktu_pemeriksaan"));
            txn.setDurasi(rs.getString("durasi"));
            txns.add(txn);
        }

        conn.con.close();

        return txns;
    }
    
    public ArrayList<EmailTransaksi> getDataWithDuration() throws SQLException, ParseException {
        ArrayList<EmailTransaksi> txns = new ArrayList();

        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "SELECT id, pengirim_email, subjek, nama_debitur, kanwil, "
                + "cabang, pic, divisi_pengirim, transaksi, "
                + "waktu_diterima, waktu_order, waktu_pemeriksaan, durasi "
                + "FROM email_transaksi "
                + "WHERE durasi != ''";

        Statement statement = conn.con.createStatement();

        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            EmailTransaksi txn = new EmailTransaksi();
            txn.setId(rs.getInt("id"));
            txn.setPengirimEmail(rs.getString("pengirim_email"));
            txn.setSubjek(rs.getString("subjek"));
            txn.setNamaDebitur(rs.getString("nama_debitur"));
            txn.setKanwil(rs.getString("kanwil"));
            txn.setCabang(rs.getString("cabang"));
            txn.setPic(rs.getString("pic"));
            txn.setDivisiPengirim(rs.getString("divisi_pengirim"));
            txn.setTransaksi(rs.getString("transaksi"));
            txn.setWaktuDiterima((Date) rs.getObject("waktu_diterima"));
            txn.setWaktuOrder(rs.getObject("waktu_order") == null ? 
                    null : (Date) rs.getObject("waktu_order"));
            txn.setWaktuPemeriksaan(rs.getObject("waktu_pemeriksaan") == null ? 
                    null : (Date) rs.getObject("waktu_pemeriksaan"));
            txn.setDurasi(rs.getString("durasi"));
            txns.add(txn);
        }

        conn.con.close();

        return txns;
    }
    
    public ArrayList<EmailTransaksi> getDataWithNoDuration() throws SQLException, ParseException {
        ArrayList<EmailTransaksi> txns = new ArrayList();

        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "SELECT id, pengirim_email, subjek, nama_debitur, kanwil, cabang, pic, divisi_pengirim, "
                + "transaksi, waktu_diterima, waktu_order "
                + "FROM email_transaksi "
                + "WHERE durasi = ''";

        Statement statement = conn.con.createStatement();

        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            EmailTransaksi txn = new EmailTransaksi();
            txn.setId(rs.getInt("id"));
            txn.setPengirimEmail(rs.getString("pengirim_email"));
            txn.setSubjek(rs.getString("subjek"));
            txn.setNamaDebitur(rs.getString("nama_debitur"));
            txn.setKanwil(rs.getString("kanwil"));
            txn.setCabang(rs.getString("cabang"));
            txn.setPic(rs.getString("pic"));
            txn.setDivisiPengirim(rs.getString("divisi_pengirim"));
            txn.setTransaksi(rs.getString("transaksi"));
            txn.setWaktuDiterima((Date) rs.getObject("waktu_diterima"));
            txn.setWaktuOrder(rs.getObject("waktu_order") == null ? null : (Date) rs.getObject("waktu_order"));

            txns.add(txn);
        }

        conn.con.close();

        return txns;
    }

    public Date getReceivedDateById(int id) throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "SELECT waktu_diterima "
                + "FROM email_transaksi "
                + "WHERE id = ?";

        PreparedStatement statement = conn.con.prepareStatement(sql);
        statement.setInt(1, id);

        ResultSet rs = statement.executeQuery();

        Date receivedDate = null;
        while (rs.next()) {
            receivedDate = rs.getDate("waktu_diterima");
        }

        conn.con.close();

        return receivedDate;
    }

    public void insert(EmailTransaksi txn) throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "INSERT INTO email_transaksi"
                + "(pengirim_email, subjek, nama_debitur, kanwil, cabang, "
                + "pic, divisi_pengirim, transaksi, waktu_diterima) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement statement = conn.con.prepareStatement(sql);
        statement.setString(1, txn.getPengirimEmail());
        statement.setString(2, txn.getSubjek());
        statement.setString(3, txn.getNamaDebitur());
        statement.setString(4, txn.getKanwil());
        statement.setString(5, txn.getCabang());
        statement.setString(6, txn.getPic());
        statement.setString(7, txn.getDivisiPengirim());
        statement.setString(8, txn.getTransaksi());
        statement.setObject(9, txn.getWaktuDiterima());

        statement.execute();

        conn.con.close();
    }

    //Update data
    public void revision(EmailTransaksi txnAfter, EmailTransaksi txnBefore) throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "UPDATE email_transaksi "
                + "SET pengirim_email = ?, subjek = ?, nama_debitur = ?, kanwil = ?, cabang = ?, pic = ?, "
                + "divisi_pengirim = ?, transaksi = ?,  waktu_diterima = ? "
                + "WHERE nama_debitur = ? AND kanwil = ? AND cabang = ? AND "
                + "divisi_pengirim = ? AND transaksi = ?";

        PreparedStatement statement = conn.con.prepareStatement(sql);
        statement.setString(1, txnAfter.getPengirimEmail());
        statement.setString(2, txnAfter.getSubjek());
        statement.setString(3, txnAfter.getNamaDebitur().equals("NA")
                ? txnBefore.getNamaDebitur() : txnAfter.getNamaDebitur());
        statement.setString(4, txnAfter.getKanwil().equals("NA")
                ? txnBefore.getKanwil() : txnAfter.getKanwil());
        statement.setString(5, txnAfter.getCabang().equals("NA")
                ? txnBefore.getCabang() : txnAfter.getCabang());
        statement.setString(6, txnAfter.getPic());
        statement.setString(7, txnAfter.getDivisiPengirim().equals("NA")
                ? txnBefore.getDivisiPengirim() : txnAfter.getDivisiPengirim());
        statement.setString(8, txnAfter.getTransaksi().equals("NA")
                ? txnBefore.getTransaksi() : txnAfter.getTransaksi());
        statement.setObject(9, txnAfter.getWaktuDiterima());
        statement.setString(10, txnBefore.getNamaDebitur());
        statement.setString(11, txnBefore.getKanwil());
        statement.setString(12, txnBefore.getCabang());
        statement.setString(13, txnBefore.getDivisiPengirim());
        statement.setString(14, txnBefore.getTransaksi());

        statement.execute();

        conn.con.close();
    }

    public void updateOrder(EmailTransaksi txn) throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "UPDATE email_transaksi "
                + "SET waktu_order = ? "
                + "WHERE id = ?";

        PreparedStatement statement = conn.con.prepareStatement(sql);
        statement.setObject(1, txn.getWaktuOrder());
        statement.setInt(2, txn.getId());

        statement.execute();

        conn.con.close();
    }

    public void updatePemeriksaanDanDurasi(EmailTransaksi txn) throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect();

        String sql = "UPDATE email_transaksi "
                + "SET waktu_pemeriksaan = ?, durasi = ? "
                + "WHERE id = ?";

        PreparedStatement statement = conn.con.prepareStatement(sql);
        statement.setObject(1, txn.getWaktuPemeriksaan());
        statement.setString(2, txn.getDurasi());
        statement.setInt(3, txn.getId());

        statement.execute();

        conn.con.close();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.EmailTransaksiDAO;
import operation.EmailOperation;
import operation.ExcelOperation;
import operation.TimeOperation;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.mail.MessagingException;
import javax.mail.Store;
import model.EmailTransaksi;
import model.EmailTransaksiProperty;

/**
 *
 * @author Asus
 */
public class MenuAwalController {

    @FXML
    private Label txnType;

    @FXML
    private TableView<EmailTransaksiProperty> tblDetails;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> from;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> subject;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmNamaDebitur;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmKanwil;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmCabang;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmPIC;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmPengirim;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmTxn;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmKet;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmWaktuEmail;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmOrderTxn;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmPemeriksaanTxn;

    @FXML
    private TableColumn<EmailTransaksiProperty, String> clmDurasi;

    @FXML
    private Button btnLogOut;

    @FXML
    private Button btnExportToExcel;

    @FXML
    private Button btnPemeriksaan;

    @FXML
    private Button btnOrder;

    @FXML
    private Button btnUndoneTxn;

    @FXML
    private Button btnDoneTxn;

    @FXML
    private Button btnAllTxn;

    private static final String TAG = "Menu Awal Controller: ";

    private EmailTransaksiDAO dbController = new EmailTransaksiDAO();

    @FXML
    public void getRowMouseClicked(MouseEvent event) {
        if (tblDetails.getSelectionModel().getFocusedIndex() != -1 || !tblDetails.getSelectionModel().isEmpty()) {
            EmailTransaksiProperty debtorTransaction = tblDetails.getSelectionModel().getSelectedItem();
            if (debtorTransaction != null) {
                if (debtorTransaction.getOrderTxn().getValue() == null) {
                    btnOrder.setDisable(false);
                    btnPemeriksaan.setDisable(true);
                } else {
                    if (debtorTransaction.getPemeriksaanTxn().getValue() == null) {
                        btnOrder.setDisable(false);
                        btnPemeriksaan.setDisable(false);
                    } else {
                        btnOrder.setDisable(true);
                        btnPemeriksaan.setDisable(false);
                    }
                }
            }
        }
    }

    @FXML
    public void btnLogOutOnAction(ActionEvent event) {
        try {
            EmailOperation emailController = new EmailOperation();
            emailController.logOut();

            Stage stage = (Stage) btnLogOut.getScene().getWindow();
            stage.close();

            LoginController loginController = new LoginController();
            loginController.showLoginView();
        } catch (MessagingException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void adjustTableView() {
        try {
            if (txnType.getText().equals("Unfinished Transactions")) {
                ArrayList<EmailTransaksi> unfinishedTxn = dbController.getDataWithNoDuration();
                showTableView(unfinishedTxn);
            } else if (txnType.getText().equals("Finished Transactions")) {
                ArrayList<EmailTransaksi> finishedTxn = dbController.getDataWithDuration();
                showTableView(finishedTxn);
            } else if (txnType.getText().equals("All Transactions")) {
                ArrayList<EmailTransaksi> allTxn = dbController.getAllData();
                showTableView(allTxn);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    public void btnOrderOnAction(ActionEvent event) {
        boolean orderStatus = false;
        boolean noData = false;

        try {
            EmailTransaksiProperty debtorTransaction = tblDetails.getSelectionModel().getSelectedItem();

            if (debtorTransaction != null) {
                EmailTransaksi choosenTxn = new EmailTransaksi();
                choosenTxn.setId(debtorTransaction.getId());

                //Get current time
                TimeOperation timeOperation = new TimeOperation();
                String currentTimeStr = timeOperation.getCurrentTime();
                Date currentTimeDate = timeOperation.setStringToDate(currentTimeStr);

                //Update table view order column
                choosenTxn.setWaktuOrder(currentTimeDate);

                //Save to database
                dbController.updateOrder(choosenTxn);

                orderStatus = true;
            } else {
                noData = true;
            }
        } catch (ParseException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        String title = "Order";
        if (orderStatus) {
            btnOrder.setDisable(false);
            btnPemeriksaan.setDisable(false);

            String context = "Transaction Ordered!";
            informationAlert(title, context);

            adjustTableView();
        } else {
            if (noData) {
                String context = "Choose a transaction!";
                warnAlert(title, context);
            } else {
                String context = "Failed to Order This Transaction!";
                errorAlert(title, context);
            }
        }
    }

    @FXML
    public void btnPemeriksaanOnAction(ActionEvent event) throws ParseException {
        boolean success = false;
        boolean noData = false;
        try {
            EmailTransaksiProperty debtorTransaction = tblDetails.getSelectionModel().getSelectedItem();

            if (debtorTransaction != null) {
                EmailTransaksi choosenTxn = new EmailTransaksi();
                choosenTxn.setId(debtorTransaction.getId());

                //Get current time
                TimeOperation timeOperation = new TimeOperation();
                String currentTimeStr = timeOperation.getCurrentTime();
                Date currentTimeDate = timeOperation.setStringToDate(currentTimeStr);

                //Get waktu diterima
                EmailTransaksiDAO dbController = new EmailTransaksiDAO();
                choosenTxn.setWaktuDiterima(dbController.getReceivedDateById(choosenTxn.getId()));

                //Update table view order column
                choosenTxn.setWaktuPemeriksaan(currentTimeDate);

                //Count time difference
                String durasi = timeOperation.countTimeDifference(choosenTxn.getWaktuDiterima(), choosenTxn.getWaktuPemeriksaan());
                choosenTxn.setDurasi(durasi);

                //Save to database
                dbController.updatePemeriksaanDanDurasi(choosenTxn);

                success = true;
            } else {
                noData = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        String title = "Pemeriksaan";
        if (success) {
            btnOrder.setDisable(false);
            btnPemeriksaan.setDisable(false);
            
            String context = "Transaction Checked!";
            informationAlert(title, context);

            adjustTableView();
        } else {
            if (noData) {
                String context = "Choose a transaction!";
                warnAlert(title, context);
            } else {
                String context = "Failed to Check This Transaction!";
                errorAlert(title, context);
            }
        }
    }

    public void showMenuAwalView() {
        try {
            FXMLLoader fxml = new FXMLLoader(getClass().getResource("/fxml/MenuAwal.fxml"));
            Parent r = (Parent) fxml.load();
            Stage s = new Stage();

            s.getIcons().add(new Image("/images/automation mail.png"));
            s.setTitle("Automasi Email");
            s.setScene(new Scene(r));
            s.setResizable(false);
            s.show();
        } catch (IOException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnUnfinishedTxnOnAction(ActionEvent event) {
        try {
            ArrayList<EmailTransaksi> txnWODuration
                    = refresh(EmailOperation.store);
            showTableView(txnWODuration);

            txnType.setText("Unfinished Transactions");
        } catch (MessagingException ex) {
            String title = "Unfinished Transaction";
            String context = "Cannot receive emails. There was a connection problem!";
            errorAlert(title, context);
        }
    }

    @FXML
    private void btnFinishedTxnOnAction(ActionEvent event) {
        try {
            ArrayList<EmailTransaksi> txnWithDuration = dbController.getDataWithDuration();
            showTableView(txnWithDuration);

            txnType.setText("Finished Transactions");
        } catch (SQLException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnAllTxnOnAction(ActionEvent event) {
        try {
            ArrayList<EmailTransaksi> txnWithDuration = dbController.getAllData();
            showTableView(txnWithDuration);

            txnType.setText("All Transactions");
        } catch (SQLException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void showTableView(ArrayList<EmailTransaksi> txns) throws MessagingException {
        MenuAwalController menuAwalCon = new MenuAwalController();

        //DEKLARASI UNTUK SI COLUMN TABLE tblDetails
        from.setCellValueFactory(cellData -> cellData.getValue().getFrom());
        subject.setCellValueFactory(c -> c.getValue().getSubject());
        clmCabang.setCellValueFactory(c -> c.getValue().getCabang());
        clmKanwil.setCellValueFactory(c -> c.getValue().getKanwil());
        clmDurasi.setCellValueFactory(c -> c.getValue().getDurasi());
        clmNamaDebitur.setCellValueFactory(c -> c.getValue().getNamaDebitur());
        clmOrderTxn.setCellValueFactory(c -> c.getValue().getOrderTxn());
        clmPIC.setCellValueFactory(c -> c.getValue().getPic());
        clmPemeriksaanTxn.setCellValueFactory(c -> c.getValue().getPemeriksaanTxn());
        clmPengirim.setCellValueFactory(c -> c.getValue().getPengirim());
        clmTxn.setCellValueFactory(c -> c.getValue().getTransaksi());
        clmWaktuEmail.setCellValueFactory(c -> c.getValue().getWaktuEmail());

        ObservableList<EmailTransaksiProperty> dtp = FXCollections.observableArrayList();
        tblDetails.setItems(dtp);

        TimeOperation timeOperation = new TimeOperation();
        for (EmailTransaksi txn : txns) {
            EmailTransaksiProperty sementara
                    = new EmailTransaksiProperty(txn.getId(), txn.getPengirimEmail(),
                            txn.getSubjek(), txn.getNamaDebitur(), txn.getKanwil(),
                            txn.getCabang(), txn.getDivisiPengirim(), txn.getTransaksi(),
                            timeOperation.setDateToString(txn.getWaktuOrder()),
                            timeOperation.setDateToString(txn.getWaktuPemeriksaan()), txn.getDurasi(),
                            txn.getPic(), timeOperation.setDateToString(txn.getWaktuDiterima()));
            dtp.add(sementara);
        }
    }

    public void saveToDatabase(Store store) throws MessagingException {
        //Get data from email
        EmailOperation emailController = new EmailOperation();
        HashMap<Long, ArrayList<EmailTransaksi>> txnsEmail = emailController.getUnseenUndraftedEmail(store);

        int newData = 0;
        int revisionData = 0;
        int failedData = 0;
        String alertTextArea = "";
        if (txnsEmail.size() > 0) {
            for (Map.Entry<Long, ArrayList<EmailTransaksi>> txnEmail : txnsEmail.entrySet()) {
                Long uid = txnEmail.getKey();
                ArrayList<EmailTransaksi> txns = txnEmail.getValue();
                try {
                    if (txns.size() > 1) {
                        EmailTransaksi txnAfter = txns.get(0);
                        EmailTransaksi txnBefore = txns.get(1);

                        dbController.revision(txnAfter, txnBefore);
                        revisionData++;
                        alertTextArea += "Revised: (" + txns.get(1).getSubjek() + ")\n";
                    } else {
                        EmailTransaksi newTxn = txns.get(0);
                        if (!newTxn.getNamaDebitur().equals("NA") || !newTxn.getKanwil().equals("NA")
                                || !newTxn.getCabang().equals("NA")
                                || !newTxn.getDivisiPengirim().equals("NA") || !newTxn.getTransaksi().equals("NA")) {
                            dbController.insert(txns.get(0));
                            newData++;
                            alertTextArea += "New Data: (" + txns.get(0).getSubjek() + ")\n";
                        }
                    }
                    emailController.setMessageFlagToDraft(store, uid);
                } catch (Exception e) {
                    failedData++;
                    alertTextArea += "Failed: (" + txns.get(0).getSubjek() + ")\n";
                }
            }

            //Alert Details!
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle("Data from email");
            alert.setHeaderText("Results!");
            alert.setContentText(newData + " new data saved, " + revisionData
                    + " data revised and " + failedData + " failed to save");

            Label label = new Label("Details:");

            TextArea textArea = new TextArea(alertTextArea);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);

            // Set expandable Exception into the dialog pane.
            alert.getDialogPane().setExpandableContent(expContent);

            alert.showAndWait();
        } else {
            String title = "Email";
            String context = "No new transaction!";
            informationAlert(title, context);
        }
    }

    public void orderTxn(EmailTransaksi txn) throws SQLException {
        try {
            TimeOperation timeOperation = new TimeOperation();

            //Get current date
            Date orderDate = timeOperation.setStringToDate(timeOperation.getCurrentTime());
            txn.setWaktuOrder(orderDate);

            //Push to database
            dbController.updateOrder(txn);
        } catch (ParseException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void checkTxn(EmailTransaksi txn) throws SQLException {
        try {
            TimeOperation timeOperation = new TimeOperation();

            //Get current date
            Date orderDate = timeOperation.setStringToDate(timeOperation.getCurrentTime());
            txn.setWaktuPemeriksaan(orderDate);

            //Push to database
            dbController.updatePemeriksaanDanDurasi(txn);
        } catch (ParseException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void btnExportOnAction(ActionEvent event) {
        String title = "Export to Excel";
        boolean exportStatus = false;
        try {
            EmailTransaksiDAO dbController = new EmailTransaksiDAO();
            ArrayList<EmailTransaksi> allTxn = dbController.getAllDataWOEmailHeader();
            ExcelOperation excelController = new ExcelOperation();
            exportStatus = excelController.exportToExcel(allTxn.toArray());
        } catch (IOException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (exportStatus) {
            String context = "File saved!";
            informationAlert(title, context);
        } else {
            String context = "Failed to save the file!";
            errorAlert(title, context);
        }
    }

    public ArrayList refresh(Store store) throws MessagingException {
        try {
            //ACCESS EMAIL INSERT IT INTO DATABASE
            saveToDatabase(store);
            return dbController.getDataWithNoDuration();
        } catch (SQLException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(MenuAwalController.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public SimpleStringProperty str2StrProp(String str) {
        SimpleStringProperty ssp = new SimpleStringProperty(str);

        return ssp;
    }

    public void warnAlert(String title, String context) {
        Alert alert = new Alert(AlertType.WARNING);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(context);
        alert.show();
    }

    public void errorAlert(String title, String context) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(context);
        alert.show();
    }

    public void informationAlert(String title, String context) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(context);
        alert.show();
    }
}

package controller;

import operation.EmailOperation;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.mail.MessagingException;

public class LoginController {

    private final String TAG = "Login Controller : ";
    
    @FXML
    private TextField txtUsername;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Button btnSignin;

    @FXML
    private Label lblErrors;

    @FXML
    public void btnSgnInOnActon(ActionEvent event) {
        loginProcess();
    }

    @FXML
    public void txtPasswordOnKeyPressed(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            loginProcess();
        }
    }

    @FXML
    public void txtUsernameOnKeyPressed(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            loginProcess();
        }
    }

    public void showLoginView() {
        try {
            FXMLLoader fxml = new FXMLLoader(getClass().getResource("/fxml/Login.fxml"));
            Parent r = (Parent) fxml.load();
            Stage s = new Stage();

            s.getIcons().add(new Image("/images/automation mail.png"));
            s.setTitle("Automasi Email");
            s.setScene(new Scene(r));
            s.show();
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void login() {
        String email = txtUsername.getText();
        String password = txtPassword.getText();

        if (email.equals("") || password.equals("")) {
            lblErrors.setTextFill(Color.TOMATO);
            lblErrors.setText("Isikan email dan password anda!");
            System.err.println(TAG + "Failed to login");
        } else {
            try {
                EmailOperation emailController = new EmailOperation();
                EmailOperation.store = 
                        emailController.connectToEmail("pop.gmail.com", "pop3", email, password);

                lblErrors.setTextFill(Color.GREEN);
                lblErrors.setText("Login Berhasil. Harap Tunggu");
                System.out.println(TAG + "Successfull Login");
            } catch (MessagingException ex) {
                ex.printStackTrace();
                if (ex.getMessage().contains("credentials")) {
                    lblErrors.setTextFill(Color.TOMATO);
                    lblErrors.setText("Email / Password yang Anda masukan Salah!");
                    System.err.println(TAG + "Wrong login");
                } else if (ex.getMessage().contains("timeout")) {
                    lblErrors.setTextFill(Color.TOMATO);
                    lblErrors.setText("Periksa Koneksi Anda!");
                    System.err.println(TAG + "Connection Problem");
                }
            }

        }
    }

    public void loginProcess() {
        // LOGIN HERE
        login();
        
        if (EmailOperation.store != null) {
            try {
                // BISA DITAMBAHAIN LOADING OR DELAY
                Stage stage = (Stage) btnSignin.getScene().getWindow();
                stage.close();
                
                MenuAwalController menuAwalCont = new MenuAwalController();
                menuAwalCont.showMenuAwalView();
            } catch (Exception e) {
                System.out.println(TAG + e.getMessage());
            }
        }
    }

}

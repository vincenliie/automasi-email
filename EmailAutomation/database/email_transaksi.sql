-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2019 at 12:22 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `transaksi_debitur`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_transaksi`
--

CREATE TABLE `email_transaksi` (
  `id` int(11) NOT NULL,
  `pengirim_email` varchar(100) NOT NULL,
  `subjek` varchar(50) NOT NULL,
  `nama_debitur` varchar(100) NOT NULL DEFAULT 'NA',
  `kanwil` varchar(5) NOT NULL DEFAULT 'NA',
  `cabang` varchar(40) NOT NULL DEFAULT 'NA',
  `pic` varchar(100) NOT NULL DEFAULT 'NA',
  `divisi_pengirim` varchar(50) NOT NULL DEFAULT 'NA',
  `transaksi` varchar(50) NOT NULL DEFAULT 'NA',
  `keterangan` varchar(255) NOT NULL,
  `waktu_diterima` datetime DEFAULT NULL,
  `waktu_order` datetime DEFAULT NULL,
  `waktu_pemeriksaan` datetime DEFAULT NULL,
  `durasi` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_transaksi`
--

INSERT INTO `email_transaksi` (`id`, `pengirim_email`, `subjek`, `nama_debitur`, `kanwil`, `cabang`, `pic`, `divisi_pengirim`, `transaksi`, `keterangan`, `waktu_diterima`, `waktu_order`, `waktu_pemeriksaan`, `durasi`) VALUES
(12, 'Vreek', 'txn pt. sejahtera 2', 'PT. Sejahtera', 'NA', 'Jakarta', 'Vreek <imabd18@gmail.com>', 'ALK', 'Terbit BG', 'Terbit BG', '2019-06-10 20:08:11', NULL, NULL, ''),
(13, 'Vreek', 'Fwd: TXN PT. Aman', 'PT. Aman Sentosa', 'NA', 'Bandung', '<ifithb2016@gmail.com>', 'ALK', 'Suku Bunga', 'Ubah Bunga', '2019-06-11 13:36:50', NULL, NULL, ''),
(14, 'Vreek', 'TXN PT. Water', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', '', '2019-06-11 14:02:06', NULL, NULL, ''),
(15, 'Vreek', 'txn pt.is', 'PT. IS', 'NA', 'Cirebon', 'NA', 'ALK', 'Ubah Bunga', 'Suku Bunga', '2019-06-11 14:40:02', NULL, NULL, ''),
(16, 'Vreek', 'txn pt.dadas', 'PT. Dadas', '2', 'Cirebon', 'NA', 'ALK', 'Ubah Bunga', 'Suku Bunga', '2019-06-11 14:47:39', NULL, NULL, ''),
(17, 'Vreek', 'Fwd: txn pt.dodol', 'PT. Dodol', '3', 'Garut', '<ifithb2016@gmail.com>', 'ALK', 'Suku Bunga', 'Ubah Bunga', '2019-06-11 15:17:19', NULL, NULL, ''),
(18, 'Vreek', 'Txn Pt. Antah Berantah', 'NA', '2', 'Cirebon', 'NA', 'ALK', 'Ubah Bunga', '', '2019-06-11 14:53:00', NULL, NULL, ''),
(19, 'Vreek', 'Fwd: Txn PT. Semua', 'PT. Semua', '10', 'Wisma Asia', '<ifithb2016@gmail.com>', 'ALK', 'Suku Bunga', 'Ubah Bunga', '2019-06-11 15:26:21', NULL, NULL, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email_transaksi`
--
ALTER TABLE `email_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email_transaksi`
--
ALTER TABLE `email_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

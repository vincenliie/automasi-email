/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author Asus
 */
public class TimeOperation {

    public static String getCurrentTime() {
        //Record date
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss a");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }
    
    public static Date setStringToDate(String dateStr) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a").parse(dateStr);
        return date;
    }

    public static String countTimeDifference(Date open, Date checked) {
        long diff = open.getTime() - checked.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String durasi = diffDays + " Hari, " + diffHours + " Jam, " + diffMinutes + " Menit, " + diffSeconds + " Detik";
        return durasi;
    }
}

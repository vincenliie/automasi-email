/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.DebtorTransaction;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;

/**
 *
 * @author Asus
 */
public class EmailController {

    public static ArrayList<DebtorTransaction> check(String host, String storeType, String user, String password) {
        ArrayList<DebtorTransaction> txs = new ArrayList();
        try {
            //create properties field
            Properties properties = new Properties();

            properties.put("mail.imap.host", host);
            properties.put("mail.imap.port", "995");
            properties.put("mail.imap.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);

            //create the IMAP store object and connect with the imap server
            Store store = emailSession.getStore("imaps");

            store.connect(host, user, password);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));
//            System.out.println("Number of unread messages: " + messages.length);

            UIDFolder uf = (UIDFolder) emailFolder; // cast folder to UIDFolder interface

            for (int i = messages.length - 1; i >= 0; i--) {
                Message message = messages[i];

                DebtorTransaction tx = new DebtorTransaction();
                if (message.getSubject().toLowerCase().contains("txn")) {
                    long messageId = uf.getUID(messages[i]); // get message Id of first message in the inbox
                    System.out.println(messageId);
                    tx.setUid(messageId);

//                    System.out.println("---------------------------------");
//                    System.out.println("Subject: " + message.getSubject());
                    tx.setSubject(message.getSubject());

                    Address froms[] = message.getFrom();
                    InternetAddress address = (InternetAddress) froms[0];
                    String sender = address.getPersonal();
//                    System.out.println("From: " + sender);
                    tx.setNamaDebitur(sender);

//                    System.out.println("Date Received: " + message.getReceivedDate());
                    tx.setWaktuEmail(message.getReceivedDate());

                    readContent(message, tx);

                    txs.add(tx);
                }
            }

            //close the store and folder objects
            emailFolder.close(false);
            store.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return txs;
    }

    public static void readContent(Part p, DebtorTransaction tx) throws Exception {
        if (p.isMimeType("text/plain")) {

            String isi = (String) p.getContent();
            //Disini ada proses pemasukkan data pada isi email kedalam class! 
            processBody(isi, tx);

        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) p.getContent();
            int count = mp.getCount();
            for (int i = 0; i < count; i++) {
                readContent(mp.getBodyPart(i), tx);
            }
        } else if (Part.ATTACHMENT.equalsIgnoreCase(p.getDisposition())) {
            // store attachment file name, separated by comma
            String fileName = p.getFileName();

            MimeBodyPart part = (MimeBodyPart) p;

            String fileDirectory = "D://content" + File.separator + fileName;
            File file = new File(fileDirectory);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                FileWriter writer = new FileWriter(file);
            } else {
                part.saveFile(fileDirectory);
            }
        }
    }

    public static void processBody(String isi, DebtorTransaction tx) {
        String lines[] = isi.split("\\r?\\n");

        for (int i = 0; i < lines.length; i++) {
            String data[] = lines[i].split(":");

            if (lines[i].equalsIgnoreCase("End")) {
                break;
            } else if (data[0].toLowerCase().contains("debitur")) {
                String debitur = "";
                if (data.length > 0) {
                    debitur = removeFirstChar(data[1]);
                }
                tx.setNamaDebitur(debitur);
//                System.out.println("Debitur = " + debitur);
            } else if (data[0].toLowerCase().contains("kanwil")) {
                String kanwil = "";
                if (data.length > 0) {
                    kanwil = removeFirstChar(data[1]);
                }
                tx.setKanwil(kanwil);
//                System.out.println("Kanwil = " + kanwil);
            } else if (data[0].toLowerCase().contains("cabang")) {
                String cabang = "";
                if (data.length > 0) {
                    cabang = removeFirstChar(data[1]);
                }
                tx.setCabang(cabang);
//                System.out.println("Cabang = " + cabang);
            } else if (data[0].toLowerCase().contains("to")) {
                String pic = "";
                if (data.length > 0) {
                    pic = removeFirstChar(data[1]);
                }
                tx.setPic(pic);
//                System.out.println("PIC = " + pic);
            } else if (data[0].toLowerCase().contains("pengirim")) {
                String pengirim = "";
                if (data.length > 0) {
                    pengirim = removeFirstChar(data[1]);
                }
                tx.setPengirim(pengirim);
//                System.out.println("Pengirim = " + pengirim);
            } else if (data[0].toLowerCase().contains("transaksi")) {
                String transaksi = "";
                if (data.length > 0) {
                    transaksi = removeFirstChar(data[1]);
                }
                tx.setTransaksi(transaksi);
//                System.out.println("Transaksi = " + transaksi);
            } else if (data[0].toLowerCase().contains("keterangan")) {
                String keterangan = "";
                if (data.length > 0) {
                    keterangan = removeFirstChar(data[1]);
                }
                tx.setKeterangan(keterangan);
//                System.out.println("Keterangan = " + keterangan);
            }
        }
    }

    public static String removeFirstChar(String str) {
        return str.substring(0, 0) + str.substring(1);
    }

    public static void setMessageFlagToSeen(String host, String storeType, String user, String password, long uid) {
        try {
            Properties properties = new Properties();

            properties.put("mail.imap.host", host);
            properties.put("mail.imap.port", "995");
            properties.put("mail.imap.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);

            //create the IMAP store object and connect with the imap server
            Store store = emailSession.getStore("imaps");

            store.connect(host, user, password);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_WRITE);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));

            UIDFolder uf = (UIDFolder) emailFolder;

            Message message = uf.getMessageByUID(uid);
            message.setFlag(Flags.Flag.SEEN, true);

            //close the store and folder objects
            emailFolder.close(false);
            store.close();
            
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(EmailController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(EmailController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Unused;

import java.util.Date;

/**
 *
 * @author Asus
 */
public class EmailContent {
    private String namaDebitur;
    private String cabang;
    private String pengirim;
    private String transaksi;
    private String keterangan;
    private Date orderTxn;
    private Date pemeriksaanTxn;
    private String durasi;

    public String getNamaDebitur() {
        return namaDebitur;
    }

    public void setNamaDebitur(String namaDebitur) {
        this.namaDebitur = namaDebitur;
    }

    public String getPengirim() {
        return pengirim;
    }

    public void setPengirim(String pengirim) {
        this.pengirim = pengirim;
    }

    public String getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(String transaksi) {
        this.transaksi = transaksi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    
        public Date getOrderTxn() {
        return orderTxn;
    }

    public void setOrderTxn(Date orderTxn) {
        this.orderTxn = orderTxn;
    }

    public Date getPemeriksaanTxn() {
        return pemeriksaanTxn;
    }

    public void setPemeriksaanTxn(Date pemeriksaanTxn) {
        this.pemeriksaanTxn = pemeriksaanTxn;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }
    
    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }
}

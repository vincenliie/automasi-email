/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Unused;

import Model.DebtorTransaction;
import java.util.Date;

/**
 *
 * @author Asus
 */
public class EmailHeader implements Comparable{
    private String pic;
    private Date waktuEmail;


    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Date getWaktuEmail() {
        return waktuEmail;
    }

    public void setWaktuEmail(Date waktuEmail) {
        this.waktuEmail = waktuEmail;
    }

    @Override
    public int compareTo(Object compareTrx) {
        Date compareDate=((DebtorTransaction)compareTrx).getWaktuEmail();
        /* For Ascending order*/
        return this.waktuEmail.compareTo(compareDate);

        /* For Descending order do like this */
        //return compareage-this.studentage;
    }
}

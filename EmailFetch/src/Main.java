
import Controller.EmailController;
import Controller.ExcelController;
import Controller.TimeOperation;
import Model.DebtorTransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class Main {

    static String host = "pop.gmail.com";// change accordingly
    static String mailStoreType = "pop3";
    static String username = "ifithb2016@gmail.com";// change accordingly
    static String password = "ifithb123";// change accordingly

    public static void main(String[] args) {
        //Get data from excel
        ArrayList<DebtorTransaction> txnExcel = new ArrayList<>();
        ExcelController xcl = new ExcelController();
        txnExcel = xcl.importFromExcel();

        //Get data from emeil
        ArrayList<DebtorTransaction> txnEmail = new ArrayList<>();
        txnEmail = EmailController.check(host, mailStoreType, username, password);

        mainInterface(txnExcel, txnEmail);
        
//        EmailController.setMessageFlagToSeen(host, mailStoreType, username, password, 47);
    }

    public static void mainInterface(ArrayList<DebtorTransaction> txnExcel, ArrayList<DebtorTransaction> txnEmail) {
        ArrayList<DebtorTransaction> allTxn = new ArrayList<>();
        allTxn.addAll(txnEmail);
        allTxn.addAll(txnExcel);

        Collections.sort(allTxn);

//        for (DebtorTransaction txn : allTxn) {
//            System.out.println(txn.getNamaDebitur() + " " + txn.getWaktuEmail());
//        }
    }

    public static void refresh(ArrayList<DebtorTransaction> txnEmail) {
        txnEmail.clear();

        txnEmail = EmailController.check(host, mailStoreType, username, password);
    }
    
    public static void openEmail(String namaDebitur, Date waktuEmail, ArrayList<DebtorTransaction> txnEmail, ArrayList<DebtorTransaction> txnExcel){
        boolean found = false;
        
        int txn_iterate = 0;
        while(txn_iterate < txnEmail.size()){
            if(txnEmail.get(txn_iterate).getNamaDebitur().equals(namaDebitur) && txnEmail.get(txn_iterate).getWaktuEmail().equals(waktuEmail)){
                txnExcel.add(txnEmail.get(txn_iterate));
                txnEmail.remove(txn_iterate);
                found = true;
            }
            txn_iterate++;
        }
        
        if(found){
            Collections.sort(txnExcel);
            ExcelController xcl = new ExcelController();
            xcl.exportToExcel(txnExcel.toArray(), "");
        }
    }
    
    public static void orderTxn(String namaDebitur, Date waktuEmail, ArrayList<DebtorTransaction> txnExcel) throws ParseException{
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        
        boolean found = false;
        
        int txn_iterate = 0;
        while(txn_iterate < txnExcel.size()){
            if(txnExcel.get(txn_iterate).getNamaDebitur().equals(namaDebitur) && txnExcel.get(txn_iterate).getWaktuEmail().equals(waktuEmail)){
                DebtorTransaction tx = txnExcel.get(txn_iterate);
                Date orderDate = TimeOperation.setStringToDate(TimeOperation.getCurrentTime());
                tx.setOrderTxn(orderDate);
                found = true;
            }
            txn_iterate++;
        }
        
        if(found){
            Collections.sort(txnExcel);
            ExcelController xcl = new ExcelController();
            xcl.exportToExcel(txnExcel.toArray(), "");
        }
    }

    public static void checkTxn(String namaDebitur, Date waktuEmail, ArrayList<DebtorTransaction> txnExcel) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        
        boolean found = false;
        
        int txn_iterate = 0;
        while(txn_iterate < txnExcel.size()){
            if(txnExcel.get(txn_iterate).getNamaDebitur().equals(namaDebitur) && txnExcel.get(txn_iterate).getWaktuEmail().equals(waktuEmail)){
                DebtorTransaction tx = txnExcel.get(txn_iterate);
                Date pemeriksaanDate = TimeOperation.setStringToDate(TimeOperation.getCurrentTime());
                tx.setPemeriksaanTxn(pemeriksaanDate);
                
                String durasi = TimeOperation.countTimeDifference(tx.getPemeriksaanTxn(), tx.getWaktuEmail());
                tx.setDurasi(durasi);
                
                found = true;
            }
            txn_iterate++;
        }
        
        if(found){
            Collections.sort(txnExcel);
            ExcelController xcl = new ExcelController();
            xcl.exportToExcel(txnExcel.toArray(), "");
        }
    }
}

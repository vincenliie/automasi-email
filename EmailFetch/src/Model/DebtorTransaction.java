/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author Asus
 */
public class DebtorTransaction implements Comparable{

    private long uid;
    private String from;
    private String subject;
    private String namaDebitur;
    private String kanwil;
    private String cabang;
    private String pengirim;
    private String transaksi;
    private String keterangan;
    private Date orderTxn;
    private Date pemeriksaanTxn;
    private String durasi;
    private String pic;
    private Date waktuEmail;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
    
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    public String getNamaDebitur() {
        return namaDebitur;
    }

    public void setNamaDebitur(String namaDebitur) {
        this.namaDebitur = namaDebitur;
    }
    
    public String getKanwil() {
        return kanwil;
    }

    public void setKanwil(String kanwil) {
        this.kanwil = kanwil;
    }

    public String getPengirim() {
        return pengirim;
    }

    public void setPengirim(String pengirim) {
        this.pengirim = pengirim;
    }

    public String getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(String transaksi) {
        this.transaksi = transaksi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    
        public Date getOrderTxn() {
        return orderTxn;
    }

    public void setOrderTxn(Date orderTxn) {
        this.orderTxn = orderTxn;
    }

    public Date getPemeriksaanTxn() {
        return pemeriksaanTxn;
    }

    public void setPemeriksaanTxn(Date pemeriksaanTxn) {
        this.pemeriksaanTxn = pemeriksaanTxn;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }
    
    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }
    
    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Date getWaktuEmail() {
        return waktuEmail;
    }

    public void setWaktuEmail(Date waktuEmail) {
        this.waktuEmail = waktuEmail;
    }

    @Override
    public int compareTo(Object compareTrx) {
        Date compareDate=((DebtorTransaction)compareTrx).getWaktuEmail();
        /* For Ascending order*/
        return this.waktuEmail.compareTo(compareDate);

        /* For Descending order*/
//        return compareDate.compareTo(this.waktuEmail);
    }
}

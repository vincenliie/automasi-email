/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MailExtractor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Flags.Flag;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MailController {

    private final static String abs_path = "poi-test.xlsx";

    private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n"; //+ org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
            }
        }
        return result;
    }

    public static void check(String host, String storeType, String user,
            String password) {
        try {
            //create properties field
            Properties properties = new Properties();

            properties.put("mail.pop3.host", host);
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.auth", true);
            properties.put("mail.pop3.ssl.enable", false);
            properties.put("mail.pop3.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);

            //create the POP3 store object and connect with the pop server
            Store store = emailSession.getStore("pop3s");

            store.connect(host, user, password);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.getMessages();
            System.out.println("messages.length---" + messages.length);

            // LOOPING UNTUK CHECKING TIAP MESSAGE
            for (Message x : messages) {
                if (!x.isSet(Flag.SEEN)) {
//                    System.out.println("");
//                    System.out.println("---------------------------------");
//                    System.out.println("Subject: " + x.getSubject());
//                    System.out.println("From: " + x.getFrom()[0]);
//                    System.out.println("Text: " + getTextFromMimeMultipart((MimeMultipart) x.getContent()));

                    String subject = x.getSubject();
                    String[] subjects = subject.split("-");

                    String content = getTextFromMimeMultipart((MimeMultipart) x.getContent());

                    if (subjects.length>1 && subjects[1].trim().equalsIgnoreCase("TRX")) {
                        String[] contents = content.split("\\r?\\n");
                        HashMap<String, String> messageProperties = new HashMap<>();

                        int p = 1;
                        while (!contents[p].equalsIgnoreCase("<ends>")) {
                            String[] info = contents[p].split("=");
                            messageProperties.put(info[0], info[1]);
                            p++;
                        }
//                        Masukkin Data untuk ke Excel

                        data2Excel(messageProperties);
//                        printHashMap(messageProperties);
                    }
                    x.setFlag(Flag.SEEN, true);
                }

            }

            //close the store and folder objects
            emailFolder.close(false);
            store.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void data2Excel(HashMap<String, String> hash) {

        File f = new File(abs_path);
        
        if (f.isFile() && f.canRead()) {
        
            FileInputStream myxls = null;
        
            Workbook workbook = null;
            
            try {
                myxls = new FileInputStream(abs_path);
                workbook = new XSSFWorkbook(myxls);
            } catch (Exception e) {
            }
            
            Sheet worksheet = workbook.getSheetAt(0);

            int lastRow = worksheet.getLastRowNum();

            int writeRow = lastRow + 1;

            Row r = worksheet.createRow(writeRow);
            String[] urutan = {"db_type", "endpoint", "db_name", "port", "username"};
            int p = 0;
            for (String x : urutan) {
                if (hash.containsKey(x)) {
//                Masukkan Datanya 
                    Cell c = r.createCell(p);
                    c.setCellValue(hash.get(x));
                } else {
                    //Masukkan Blank
                    Cell c = r.createCell(p);
                    c.setCellValue("");
                }
                p++;
            }

            try {
                myxls.close();
                FileOutputStream output_file = new FileOutputStream(new File(abs_path));
                workbook.write(output_file);
                output_file.close();
            } catch (IOException e) {
            }

            //write changes
            System.out.println("Data is successfully written");
        } else {
            Workbook wb = new XSSFWorkbook();
            wb.createSheet();
            
            try {
                FileOutputStream out = new FileOutputStream(new File(abs_path));
                wb.write(out);
                out.close();
            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }
            data2Excel(hash);
        }
    }

    private static void printHashMap(HashMap<String, String> hash) {
        for (Map.Entry<String, String> entry : hash.entrySet()) {
            Object key = entry.getKey();
            Object value = entry.getValue();

            System.out.println("Key : " + key);
            System.out.println("Value : " + value);
            System.out.println("--------------------------------");
            System.out.println("");
        }
    }
}
